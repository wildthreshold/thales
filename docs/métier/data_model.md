# Thales - Cyber Threat Handbook - Modèle de données

## Périmètre

Ce document défini le modèle des données métiers côté serveur Drupal pour le projet CyberThreat pour Thales.

Il a été défini à partir de :
- des besoins fonctionnels exprimés dans les documents :
  - *Note de Cadrage Handbook III_The Cyber Threat Atlas.pptx*
  - *Thales-Resume-Dev.docx*
- du document Thales :
  - *THALES_CTI_ATK_DATA_MODEL_v1.pdf*
- des fichiers JSON définissant les attaques et les campagnes d'attaques


## Modèle

### adversary

Décrit un groupe d'attaquants (attaquants ayant des comportements et des ressources similaires).

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique du groupe d'attaquants        |            |
| name             | string       | oui        | Nom du groupe d'attaquants                          |            |
| id_thales        | string       | oui        | Identifiant interne Thales                          |            |
| version          | int          | oui        | Version                                             |            |
| created          | date         | oui        | Date de la création du groupe                       |            |
| modified         | date         | oui        | Date de la dernière modification                    |            |
| description      | string       | oui        | Code Html décrivant le groupe                       |            |
| types            | id list      | oui        | Liste des types d'attaquants                        |            |
| target_sector    | id list      | oui        | Liste des secteurs cibles                           |            |
| target_country   | id list      | oui        | Liste des id des pays cibles                        |            |
| origin           | id list      | oui        | Liste des id des pays origines                      |            |
| motivation       | id list      | oui        | Liste des motivations des attaquants                |            |
| languages        | id list      | oui        | Liste des langues des attaquants                    |            |
| alias            | string list  | oui        | Liste des alias nommant le groupe                   |            |
| attack_patterns  | id list      | oui        | Liste des schémas d'attaques                        |            |
| malwares         | id list      | oui        | Liste des malwares utilisés                         |            |
| tools            | id list      | oui        | Liste des outils utilisés                           |            |
| vulnerabilities  | id list      | oui        | Liste des vulnérabilités cibles                     |            |
| campaigns        | id list      | oui        | Liste des campagnes attribuées                      |            |
| analyze          | string       |            | Code Html décrivant l'analyse du groupe             |            |
| categories       | id list      |            | Liste des catégories associées au groupe            |            |
| hashtags         | id list      |            | Liste des hashtags associés au groupe               |            |
| references       | url list     | oui        | Liste des url des références                        |            |


### type

Décrit un type d'attaquant

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique du type d'attaquant           |            |
| name             | string       | oui        | Nom du type d'attaquant                             |            |
| rgb              | string       |            | Code hexa de la couleur du type d'attaquant         |            |


### target_sector

Décrit un secteur cible

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique du secteur                    |            |
| name             | string       | oui        | Nom du secteur cible                                |            |
| icon             | file         |            | Icône illustrant le secteur                         |            |


### motivation

Décrit une motivation d'attaque

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique de la motivation              |            |
| name             | string       | oui        | Nom de la motivation d'attaque                      |            |


### language

Décrit une langue d'un attaquant

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique de la langue                  |            |
| name             | string       | oui        | Nom de la langue                                    |            |


### country

Décrit un pays

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique du pays                       |            |
| iso              | string       |            | Code ISO du pays                                    |            |
| name             | string       | au besoin  | Nom du pays                                         |            |
| area             | id           |            | Id de la zone de rattachement du pays               |            |
| center           | lat/lon      |            | Latitude, longitude du centre du pays               |            |
| flag             | file         |            | Image du drapeau du pays                            |            |


### area

Décrit une zone géographique

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique de la zone                    |            |
| name             | string       |            | Nom de la zone géographique                         |            |
| analyze          | string       |            | Code Html décrivant l'analyse de la zone            |            |


### attack_pattern

Décrit un schéma d'attaque

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique du schéma                     |            |
| name             | string       | oui        | Nom du schéma                                       |            |
| code             | string       | oui        | Identifiant T du schéma                             |            |
| link             | string       |            | Lien vers la référence Mitre                        |            |
Le lien est construit comme suit `https://attack.mitre.org/techniques/{code}/`


### malware

Décrit un malware

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique du malware                    |            |
| name             | string       | oui        | Nom du malware                                      |            |


### tool

Décrit un outil

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique de l'outil                    |            |
| name             | string       | oui        | Nom de l'outil                                      |            |


### vulnerability

Décrit une vulnérabilité

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique de la vulnérabilité           |            |
| name             | string       | oui        | Nom de la vulnérabilité                             |            |
| link             | string       |            | Lien vers la référence NVD                          |            |
Le lien est construit comme suit `https://nvd.nist.gov/vuln/detail/{name}`


### campaign

Décrit une campagne

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique de la campagne                |            |
| name             | string       | oui        | Nom de la campagne                                  |            |
| id_adversary     | id           | oui        | Identifiant du groupe d'attaquant                   |            |
| other_adversary  | string list  | oui        | Liste des noms d'autres attaquants associés         |            |
| id_thales        | string       | oui        | Identifiant interne Thales de la campagne           |            |
| version          | int          | oui        | Version                                             |            |
| created          | date         | oui        | Date de la création du groupe                       |            |
| modified         | date         | oui        | Date de la dernière modification                    |            |
| description      | string       | oui        | Code Html décrivant la campagne                     |            |
| happened_at      | date         | oui        | Date d'apparation de la campagne                    |            |
| attack_patterns  | id list      | oui        | Liste des schémas d'attaques                        |            |
| malwares         | id list      | oui        | Liste des malwares utilisés                         |            |
| tools            | id list      | oui        | Liste des outils utilisés                           |            |
| vulnerabilities  | id list      | oui        | Liste des vulnérabilités cibles                     |            |
| sources          | id list      | oui        | Liste des sources avec leur couleur                 |            |


### category

Décrit une catégorie (sémantique)

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique de la catégorie               |            |
| name             | string       |            | Nom de la catégorie                                 |            |
| description      | string       |            | Description de la catégorie                         |            |


### hashtag

Décrit un hashtag (sémantique)

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique du hashtag                    |            |
| name             | string       |            | Nom du hashtag                                      |            |
| description      | string       |            | Description du hashtag                              |            |


### expert

Décrit un expert ou un témoignage

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique de l'expert, du témoignage    |            |
| last name        | string       |            | Nom de l'expert, du témoin                          |            |
| first name       | string       |            | Prénom de l'expert, du témoin                       |            |
| type             | string       |            | Type d'expert, de témoin                            |            |
| title            | string       |            | Titre de l'expert, du témoignage                    |            |
| organization     | string       |            | Organisation de l'expert, du témoin                 |            |
| country          | id           |            | Identifiant du pays de l'expert, du témoin          |            |
| description      | string       |            | Description Html de l'expert, du témoignage         |            |
| location         | lat/lon      |            | Coordonnées géographiques de l'expert, du témoin    |            |
| photo            | file         |            | Photo de l'expert, du témoin                        |            |
| link             | url          |            | Lien externe ou interne vers l'expert               |            |

Les types d'experts, de témoins possibles sont :
- Thales Cyber Teams
- Thales SOC
- Speakers Bureau
- Testimony


### report

Décrit un rapport

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique du rapport                    |            |
| name             | string       |            | Nom du rapport                                      |            |
| zone             | id           |            | Identifiant de la zone associée                     |            |
| file             | file         |            | Fichier PDF du rapport                              |            |


### source

Décrit une source sur une attaque

| Propriété        | Type         | Importé    | Description                                         | Etat       |
|------------------|--------------|------------|-----------------------------------------------------|------------|
| id               | id           |            | Identifiant technique de la source                  |            |
| name             | string       | oui        | Nom de la source                                    |            |
| color            | string       | oui        | Nom de la couleur de la source                      |            |
| rgb              | string       |            | Code hexa de la couleur de la source                |            |
Le code rgb de la couleur est inféré depuis le nom de la couleur
