
# Proposition d'animations pour la page d'accueil


## Cadre

* Basée sur des références réelles.
* Pas 2 attaques depuis un même pays
* Les points origine et cibles ne sont pas trop proches pour bien montrer une animation


## Animations

### 01
* Actor : ATK1
* Origin : China
* Targets : United States Of America, Canada, France, Japan
* Type : State Sponsored
* Motivations : Information theft, Espionage

### 02
* Actor : ATK8
* Origin : France
* Targets : United States Of America, Congo (Democratic Republic Of The), Malaysia, Iran (Islamic Republic Of)
* Type : not defined
* Motivations : Espionage

### 03
* Actor : ATK11
* Origin : India
* Targets : United States Of America, United Kingdom Of Great Britain And Northern Ireland, Japan
* Type : State Sponsored
* Motivations : Information theft, Espionage

### 04
* Actor : ATK17
* Origin : Vietnam
* Targets : Australia, United States Of America, Germany
* Type : State Sponsored
* Motivations : Espionage

### 05
* Actor : ATK27
* Origin : Lebanon
* Targets : Viet Nam, Venezuela (Bolivarian Republic Of), United States Of America, Thailand, Russian Federation
* Type : State Sponsored
* Motivations : Ideology, Financial Gain, Coercion

### 06
* Actor : ATK103
* Origin : Russia
* Targets : United States Of America, United Arab Emirates, Italy
* Type : Cyber Criminal
* Motivations : Financial Gain

### 07
* Actor : ATK117
* Origin : North Korea
* Targets : Mexico, Russian Federation, Brazil
* Type : Cyber Criminal
* Motivations : Financial Gain

### 08
* Actor : ATK128
* Origin : Saudi Arabia
* Targets : United States Of America, United Kingdom Of Great Britain And Northern Ireland
* Type : Hacktivist
* Motivations : Revenge, Personal Satisfaction, Financial Gain, Dominance, Coercion

### 09
* Actor : ATK132
* Origin : Syria
* Targets : United States Of America, United Kingdom Of Great Britain And Northern Ireland
* Type : Cyber Terrorist
* Motivations : Revenge, Organizational Gain, Notoriety, Ideology, Dominance, Coercion
