
# Importation des données Thales Cyberthreat dans le site Drupal

## Utilisation

1. Copier le dossier `docs/data` contenant tous les JSON dans `web/sites/default/files`
2. Lancer le flux d'importation `Adversary import from data folder` (l'adresse du dossier est `public://data/attacks`)
3. Lancer le flux d'importation `Campaign import from data folder` (l'adresse du dossier est `public://data/attacks`)
4. Lancer le flux d'importation `Area import from JSON` (l'adresse du dossier est `public://data/areas`)


## Extensions utilisées
* Feeds
* Feeds Extensible Parsers


## Mécanisme utilisé
* Parser JSON JMESPath (qui semble être le plus complet même si la grammaire est quelque peu absconse)
  * Grammaire : https://jmespath.org/specification.html
  * Exemples : https://jmespath.org/examples.html
  * Testeur : https://jmespath.org/


## Règles JMESPath configurées pour l'importation dans Drupal

### Attaquants

#### Contexte
* ```objects[?object_type==`ThalesAdversary`]```

#### Mapping
* Nom
  * ```name```
* Id Thales
  * ```id```
* Créée le
  * ```created```
* Modifiée le
  * ```modified```
* Description
  * ```description```
* Version
  * ```version```
* Type d'attaquant
  * ```types_of_attacker|[0]```
* Alias
  * ```related_adversaries[]|[?relationship==`alias`].name|sort(@)```
* Secteurs cibles
  * ```attributes[]|[?name==`Target Sector`].value|sort(@)```
* Pays cibles
  * ```attributes[]|[?name==`Target Country`].value|sort(@)```
* Pays origines
  * ```attributes[]|[?name==`Origin`].value```
* Motivations
  * ```attributes[]|[?name==`Motivation`].value|sort(@)```
* Langues
  * ```attributes[]|[?name==`Language`].value|sort(@)```
* Attack patterns
  * ```related_attack_patterns[]|[?relationship==`uses`].name|sort(@)```
* Malwares
  * ```related_malwares[]|[?relationship==`uses`].name|sort(@)```
* Tools
  * ```related_tools[]|[?relationship==`uses`].name|sort(@)```
* Vulnérabilités
  * ```related_vulnerabilities[]|[?relationship==`targets`].name|sort(@)```
* Références
  * ```attributes[]|[?name==`Reference`].value|sort(@)```


### Campagnes

#### Contexte
* ```objects[?object_type==`ThalesCampaign`]```

#### Mapping
* Nom
  * ```name```
* Id Thales
  * ```id```
* Survenue le
  * ```happened_at```
* Créée le
  * ```created```
* Modifiée le
  * ```modified```
* Description
  * ```description```
* Version
  * ```version```
* Attaquant principal
  * ```[related_adversaries|[?relationship==`attributed-to` && contains(name,`ATK`)].name]|[]```
* Autres attaquants (dont l'attaquant principal)
  * ```[related_adversaries|[?relationship==`attributed-to`].name|sort(@)]|[]```
* Attack patterns
  * ```[related_attack_patterns[]|[?relationship==`uses`].name|sort(@)]|[]```
* Malwares
  * ```[related_malwares[]|[?relationship==`uses`].name|sort(@)]|[]```
* Tools
  * ```[related_tools[]|[?relationship==`uses`].name|sort(@)]|[]```
* Vulnérabilités
  * ```[related_vulnerabilities[]|[?relationship==`targets`].name|sort(@)]|[]```
* Sources
  * ```[sources[]|[?object_type==`ThalesSource`].name|sort(@)]|[]```


## TODO

* Lister les campagnes dans les attaquants (champ `field_campaigns_adversary`)
  * Possiblement pas réalisable à cause de la structure "bizarre" des fichiers JSON (plusieurs campagnes sont listées au même niveau que les informations de l'attaquant, et non dans l'attaquant, le parser perd ses petits)


## ~~Erreur rencontrée lors des importations~~

En utilisant le schéma d'URL `public://` cette erreur n'est plus rencontrée.


### Erreur liée à la lecture d'un flux depuis un répertoire Windows

Contexte : on veut lire tous les fichiers JSON contenus dans le dossier `data` à la racine web.

Lors de la soumission du formulaire 2 éléments sont vérifiés :
* que le chemin d'accès est bien une URL avec un protocole, un host
* que le dossier fourni existe

Erreurs rencontrées :
* soit l'URL indiquée est valide (comme `file://data` avec un chemin relatif à la racine web ou `file://c/Dev/thales/web/data` avec un chemin absolu)
  * alors la fonction PHP is_dir() ne valide par l'URL comme un dossier local
* soit l'URL indiquée n'est pas valide (comme `file://c:\Dev\thales\web\data`)
  * alors la fonction PHP is_dir() valide l'URL comme un dossier local

Pour contourner le problème rapidement il faut modifier le fichier `web/modules/contrib/feeds/src/Element/Uri.php` après la ligne 41 et forcer `$valid` à `true`

```php
  /**
   * Form element validation handler for #type 'feeds_uri'.
   */
  public static function validateUrl(&$element, FormStateInterface $form_state, &$complete_form, StreamWrapperManagerInterface $stream_wrapper_manager = NULL) {
    if (empty($stream_wrapper_manager)) {
      $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager');
    }

    $value = $stream_wrapper_manager->normalizeUri(trim($element['#value']));
    $form_state->setValueForElement($element, $value);

    if (!$value) {
      return;
    }

    $parsed = parse_url($value);
    $valid = $parsed && !empty($parsed['scheme']) && !empty($parsed['host']);
    $valid = true;  // <-- SLR : on force la validation de l'URL quand bien même elle ne peut pas être parsée

    if (!$valid) {
      $form_state->setError($element, t('The URI %url is not valid.', ['%url' => $value]));
      return;
    }

    if ($element['#allowed_schemes'] && !in_array(static::getScheme($value), $element['#allowed_schemes'], TRUE)) {
      $args = [
        '%scheme' => static::getScheme($value),
        '@schemes' => implode(', ', $element['#allowed_schemes']),
      ];
      $form_state->setError($element, t("The scheme %scheme is invalid. Available schemes: @schemes.", $args));
    }
  }
```
