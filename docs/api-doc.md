# API Documentation  

---

## Référence des endpoints contenu :

### Adversary :
`/api/adversaries` : Liste de tous les adversaires.  
#### query params :
- `?nid=<nid>` : Affiche un adversaire par son node id  
- `?name=<adversary_name>` : Affiche un adversaire par son nom  
- `?attackers_type=<attackers_type_id>` : La liste des adversaires trié par type d'attaquant  (id tu terme de taxonomie Adversary type)


### Campaign :
`/api/campaigns` : Liste de toutes les campagnes.  
#### query params :
- `?nid=<nid>` : Affiche une campagne par son node id  



### Expert :
`/api/experts` : Liste de tous les experts.  
#### query params :


### Areas :
`/api/areas` : Renvoi l'ensemble des Areas
#### query params :
- `?nid=<nid>` : Affiche un une area par son node id

### News :
`/api/news` : Renvoi l'ensemble des news.  
#### query params :
- `?adversaries=<adversary id>` : La liste des news trié par adversaire
- `?area=<area id>` : La liste des news trié par area


---

## Référence des endpoints taxonomie :

`/api/adversary-type` : Liste des types d'adversaires  
`/api/countries` : Liste des pays  
`/api/motivation` : Liste des motivations  
`/api/target-sector` : Liste des secteurs cible  