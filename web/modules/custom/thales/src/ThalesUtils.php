<?php

namespace Drupal\thales;



class ThalesUtils
{
	
	// const APP_FRONT_DEV = "http://localhost:3000/";
	// const APP_FRONT_STAGING = "https://app-front.teststaging.online/";
	// const APP_FRONT_PROD = "";
	
	
	/**
	 * /////////////////////////////////////////////////////////////////////////
	 * // TAXONOMY UTILS :
	 * /////////////////////////////////////////////////////////////////////////
	 */
	
	/**
	 * Utility: find term by name and vid.
	 * @param null $name
	 * @param null $vid
	 * @return int
	 */
	public static function getTidByName(string $name = NULL, string $vid = NULL) {
		
		$properties = [];
		
		if (!empty($name)) { $properties['name'] = $name; }
		if (!empty($vid))  { $properties['vid'] = $vid; }
		
		$terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties($properties);
		$term = reset($terms);
		
		return !empty($term) ? $term->id() : 0;
	}
	
	
	
	public static function getTaxonomyNameByTid($tid) {
		if (\Drupal\taxonomy\Entity\Term::load($tid)) {
			return \Drupal\taxonomy\Entity\Term::load($tid)->getName();
		} else {
			return;
//			return "nope ----> " . $tid;
		}
	}
	
	
	public static function getTaxTree(string $vid, int $tid_parent = 0, int $max_depth = 1,  bool $output = TRUE) {
		return \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree(
			$vid,        // This is your taxonomy term vocabulary (machine name).
			$tid_parent, // This is "tid" of parent. Set "0" to get all.
			$max_depth,  // Get terms from 1st levels.
			$output      // Get full load of taxonomy term entity.
		);
	}
	
	
	/**
	 * Retourne le parent d'un term de taxo
	 * @param int $tid
	 * @return int|mixed
	 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
	 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
	 */
	public static function getParentByTid(int $tid) {
		
		$parents = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadParents($tid);
		$parent = reset($parents);
		
		return !empty($parent) ? $parent : 0;
	}
	
	/**
	 * Retourne le nom du parent d'un term de taxo
	 * @param int $tid
	 * @return int|mixed
	 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
	 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
	 */
	public static function getParentNameByTid(int $tid) {
		
		$parents = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadParents($tid);
		$parent = reset($parents);
		
		return !empty($parent) ? $parent->get("name")->value : "";
	}
	
	
	
	
	/**
	 * /////////////////////////////////////////////////////////////////////////
	 * // CONTENT TYPE UTILS :
	 * /////////////////////////////////////////////////////////////////////////
	 */
	
	public static function getNodeName(int $nid) {
		$node = \Drupal::entityTypeManager()->getStorage('node');
		$name = $node->load($nid);
//		return $name->get("title")->value;
		return !empty($name) ? $name->get("title")->value : null;
	}
	
	
	
	
	/**
	 * /////////////////////////////////////////////////////////////////////////
	 * // STRING UTILS :
	 * /////////////////////////////////////////////////////////////////////////
	 */
	public static function slugify(string $text, string $divider = '-'):string {
		
		$text = preg_replace('~[^\pL\d]+~u', $divider, $text); // replace non letter or digits by divider
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text); // transliterate
		$text = preg_replace('~[^-\w]+~', '', $text); // remove unwanted characters
		$text = trim($text, $divider); // trim
		$text = preg_replace('~-+~', $divider, $text); // remove duplicate divider
		$text = strtolower($text); // lowercase
		
		if (empty($text)) { return 'n-a'; }
		
		return $text;
	}
}