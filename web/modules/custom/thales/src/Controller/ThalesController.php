<?php

namespace Drupal\thales\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for thales routes.
 */
class ThalesController extends ControllerBase {

    /**
     * Builds the response.
     */
    public function build() {

        $build['content'] = [
            '#type' => 'item',
            '#markup' => $this->t('It works!'),
        ];

        return $build;
    }



    public function home() {
        return [ '#type' => 'markup', ];
    }

}
