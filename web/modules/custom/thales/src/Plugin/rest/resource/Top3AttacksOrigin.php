<?php

namespace Drupal\thales\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\thales\ThalesUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "top3attacks_origin",
 *   label = @Translation("Top3attacks origin"),
 *   uri_paths = {
 *     "canonical" = "/api/top-3-attacks-origin"
 *   }
 * )
 */
class Top3AttacksOrigin extends ResourceBase {

    /**
     * Responds to GET requests.
     * @param string $payload
     * @return \Drupal\rest\ResourceResponse
     */
	public function get():ResourceResponse {
		
		$entities = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple();
		$targeted_country_adversary = [];
		foreach ($entities as $entity) {
			if ($entity->bundle() == "adversary") {
				$field_target_country_adversary = $entity->get('field_origin_country_adversary')->referencedEntities();
				foreach ($field_target_country_adversary as $tc) {
					array_push($targeted_country_adversary, $tc->get("tid")->value);
				}
			}
		}
		
		$targeted_country_to_area = [];
		foreach ($targeted_country_adversary as $tca) {
			$ar = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
				"type" => "area",
				"field_countries" => $tca
			]);
			if (!empty($ar)) {
				array_push($targeted_country_to_area, reset($ar)->nid->value);
			}
		}
		
		$top_3 = [];
		foreach ($this->getCountriesSortedByOccurrence($targeted_country_to_area) as $v) {
			array_push($top_3, ThalesUtils::getNodeName($v));
		}

		$top_3 = array_slice($top_3, 0, 3);
		
		return new ResourceResponse(["top_3_attacks_origin" => $top_3]);
	}
	
	
	public function getCountriesSortedByOccurrence (array $arr):array {
		$count =  array_count_values($arr); // Compte le nombre d'occurences (supprime les doublons) ex: [ "Aviation" => 5, "Chemicals" => 4, "Communication" => 11, ... ]
		asort($count); // tri par ordre croissant. ex: [ "Chemicals" => 4, "Aviation" => 5, "Communication" => 11, ... ]
		$count = array_reverse($count, true); // reverse...
		return array_keys($count); // récupère les trois premiers.
	}

}
