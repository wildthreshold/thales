<?php

namespace Drupal\thales\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\thales\ThalesUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "top3targeted_sectors_for_area",
 *   label = @Translation("Top3targeted sectors for area"),
 *   uri_paths = {
 *     "canonical" = "/api/top-3-targeted-sectors-for-area"
 *   }
 * )
 */
class Top3TargetedSectorsForArea extends ResourceBase {
	
    /**
     * @return \Drupal\rest\ResourceResponse
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
	public function get() {
		
		if ( !empty(reset(\Drupal::request()->query))
			&& \Drupal::request()->get('area') != ""
			&& array_key_exists("area", reset(\Drupal::request()->query))
		) {
			// =====================================================================
			// Récupération des pays de l'area communiqué en query param :
			// =====================================================================
			$area = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
				"title" => \Drupal::request()->query->get('area'),
				"type" => "area"
			]);
			$area_countries = reset($area)->get("field_countries")->getValue();
			$area_countries_parsed = [];
			foreach ($area_countries as $v) {
				if (ThalesUtils::getTaxonomyNameByTid($v["target_id"]) !== null) {
					array_push($area_countries_parsed, $v["target_id"]);
				}
			}
			
			if (!empty($area_countries_parsed)) {
				
				$target_sector_parent = ThalesUtils::getTaxTree("target_sector");
				$adversary_type = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(["vid" => "type_attaquant"]);
				$DATA = [];
				
				foreach ($target_sector_parent as $ts) {
					foreach ($adversary_type as $adv_type) {
						$adversary_for_type = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
							"type" => "adversary",
							'field_types_adversary' => $adv_type->get("tid")->value,
							'field_target_sector_adversary' => $ts->get("tid")->value,
							'field_target_country_adversary' => $area_countries_parsed,
						]);
						foreach ($adversary_for_type as $key => $adv) {
							$DATA[$ts->get("name")->value][] = ThalesUtils::slugify($adv_type->get("name")->value, "_");
						}
					}
				}
				
				array_multisort($DATA, SORT_DESC);
				$top_3 = array_slice($DATA, 0, 3);
				$top_3 = array_keys($top_3);
				
				$response = new ResourceResponse([ "top_3_targeted_sectors_for_area" => $top_3 ]);
				$response->addCacheableDependency($top_3);
				return $response;
			}
			
			$response = new ResourceResponse(["no data"]);
			$response->addCacheableDependency(["no data"]);
			return $response;
			
		} else {
			$response = new ResourceResponse(["no data"]);
			$response->addCacheableDependency(["no data"]);
			return $response;
		}
		
	}
	
	
	public function getTargetedSectorsSortedByOccurence (array $arr):array {
		$count =  array_count_values($arr);
		asort($count);
		$count = array_reverse($count);
		return array_keys($count);
	}

}
