<?php

namespace Drupal\thales\Plugin\rest\resource;

use Drupal\rest\Annotation\RestResource;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use Psr\Log\LoggerInterface;
use Drupal\Core\Session\AccountProxyInterface;

use Drupal\views\Views;

use Drupal\thales\ThalesUtils;



/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "example_get_rest_resource",
 *   label = @Translation("Example get rest resource"),
 *   uri_paths = {
 *     "canonical" = "/example-rest"
 *   }
 * )
 */
class ExampleGetRestResource extends ResourceBase {
	
	
	/**
	 * Responds to GET requests.
	 *
	 * Returns a list of bundles for specified entity.
	 *
	 * @throws \Symfony\Component\HttpKernel\Exception\HttpException
	 *   Throws exception expected.
	 */
	public function get() {
		
		
//		$view = Views::getView('data_areas_taxonomy');
////		$display = $view->getDisplay('rest_export_nested_1');
//		$view->setDisplay('rest_export_nested_1');
//		$view->execute();
//		dump(views_get_view_result("data_areas_taxonomy", "rest_export_nested_1"));
		
		
		
		
		$result = "toto";
		$response = new ResourceResponse($result);
//		$response->addCacheableDependency($result);
		return $response;
	}
	
	
}