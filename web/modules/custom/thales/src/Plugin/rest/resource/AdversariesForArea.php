<?php

namespace Drupal\thales\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use Drupal\thales\ThalesUtils;
use Drupal\svg_image\Plugin\Field\FieldFormatter\SvgImageFormatter;


/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "adversaries_for_area",
 *   label = @Translation("Adversaries for area"),
 *   uri_paths = {
 *     "canonical" = "/api/area-adversaries"
 *   }
 * )
 */
class AdversariesForArea extends ResourceBase {

    /**
     * @return \Drupal\rest\ResourceResponse
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function get() {
	
		if ( !empty(reset(\Drupal::request()->query))
			&& \Drupal::request()->get('area') != ""
			&& array_key_exists("area", reset(\Drupal::request()->query))
		) {
			// =====================================================================
			// Récupération AREA :
			// =====================================================================
			$area = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
				"title" => \Drupal::request()->query->get('area'),
				"type" => "area"
			]);
			$area_countries = reset($area)->get("field_countries")->getValue();
			$area_countries_parsed = [];
			foreach ($area_countries as $v) {
				if (ThalesUtils::getTaxonomyNameByTid($v["target_id"]) !== null) {
					array_push($area_countries_parsed, $v["target_id"]);
				}
			}

			if (!empty($area_countries_parsed)) {
				
				$adversary_type = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(["vid" => "type_attaquant"]);
				$target_sector_parent = ThalesUtils::getTaxTree("target_sector");
				
				$DATA = [];
				$adv_type_for_chart = [];
				foreach ($target_sector_parent as $ts) {
					foreach ($adversary_type as $adv_type) {
						$adversary_for_type = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
							"type" => "adversary",
							'field_types_adversary' => $adv_type->get("tid")->value,
							'field_target_sector_adversary' => $ts->get("tid")->value,
							'field_target_country_adversary' => $area_countries_parsed,
						]);
						foreach ($adversary_for_type as $key => $adv) {
							$DATA[$ts->get("name")->value]["icon_adv_type"] = $ts->get("field_picto_target_sec")->view("inline")[0]["#svg_data"];
							$DATA[$ts->get("name")->value][ThalesUtils::slugify($adv_type->get("name")->value, "_")][] = $adv->get("title")->value;
							//					$DATA[$ts->get("name")->value][$adv_type->get("name")->value][$key]["title"] = $adv->get("title")->value;
							//					$DATA[$ts->get("name")->value][$adv_type->get("name")->value][$key]["type"] = $adv->get("field_types_adversary")->first()->get('entity')->getTarget()->get("name")->value;
							//					$DATA[$ts->get("name")->value][$adv_type->get("name")->value][$key]["target_sector"] = $adv->get("field_target_sector_adversary")->referencedEntities();
							
							array_push($adv_type_for_chart, $adv_type->get("name")->value);
						}
						//					$DATA[$adv_type->get("name")->value] = $adversary_for_type;
					}
				}
				
				$adv_type_for_chart = array_count_values($adv_type_for_chart);
				$DATA["count_adv_type"] = $adv_type_for_chart;
//				dump($DATA);
				
				$response = new ResourceResponse($DATA);
				$response->addCacheableDependency($DATA);
				return $response;
			}
			
			$response = new ResourceResponse(["no data"]);
			$response->addCacheableDependency(["no data"]);
			return $response;
			
		} else {
			$response = new ResourceResponse(["no data"]);
			$response->addCacheableDependency(["no data"]);
			return $response;
		}
	
    }

}
