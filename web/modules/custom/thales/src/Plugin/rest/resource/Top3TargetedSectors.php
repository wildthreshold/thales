<?php

namespace Drupal\thales\Plugin\rest\resource;


use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

use Drupal\thales\ThalesUtils;

/**
 * @RestResource(
 *   id = "top3targeted_sectors",
 *   label = @Translation("Top3targeted sectors"),
 *   uri_paths = {
 *     "canonical" = "/api/top-3-targeted-sectors"
 *   }
 * )
 */
class Top3TargetedSectors extends ResourceBase {
	
	/**
	 * Responds to GET requests.
	 * @param string $payload
	 * @return \Drupal\rest\ResourceResponse
	 */
	public function get():ResourceResponse {
		
		$entities = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple();
		$targeted_sectors = [];
		foreach ($entities as $entity) {
			if ($entity->bundle() == "adversary") {
				$field_target_sector_adversary = $entity->get('field_target_sector_adversary')->referencedEntities();
				foreach ($field_target_sector_adversary as $ts) {
					array_push($targeted_sectors, $ts->label());
				}
			}
		}
		
		$top_3 = [];
		foreach ($this->getTop3TargetedSectors($targeted_sectors) as $v) {
			$parent_term_name = ThalesUtils::getTidByName($v);
			if (ThalesUtils::getParentNameByTid($parent_term_name) && !in_array(ThalesUtils::getParentNameByTid($parent_term_name), $top_3)) {
				array_push($top_3, ThalesUtils::getParentNameByTid($parent_term_name));
			}
		}
		
		return new ResourceResponse(["top_3_targeted_sectors" => array_slice($top_3, 0, 3)]);
	}
	
	
	public function getTop3TargetedSectors (array $arr):array {
		$count =  array_count_values($arr); // Compte le nombre d'occurences (supprime les doublons) ex: [ "Aviation" => 5, "Chemicals" => 4, "Communication" => 11, ... ]
		asort($count); // tri par ordre croissant. ex: [ "Chemicals" => 4, "Aviation" => 5, "Communication" => 11, ... ]
		$count = array_reverse($count); // reverse...
		// return array_keys(array_slice($count, 0, 3)); // récupère les trois premiers.
		return array_keys($count);
	}
	
	
}
