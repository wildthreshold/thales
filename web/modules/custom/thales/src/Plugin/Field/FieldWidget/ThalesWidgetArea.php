<?php

namespace Drupal\thales\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'field_example_text' widget.
 *
 * @FieldWidget(
 *   id = "thales_custom_countries_map_position_area_widget",
 *   module = "thales",
 *   label = @Translation("Custom countries map position for areas"),
 *   field_types = { "thales_custom_countries_map_position_area" }
 * )
 */
class ThalesWidgetArea extends WidgetBase {
	
	/**
	 * {@inheritdoc}
	 */
	public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
		$element['#type'] = 'fieldset';
		
		$element['countries_map_svg'] = [
			'#type' => 'text_format',
			'#format' => 'restricted_html',
			'#title' => $this->t('Map SVG for area countries.'),
			'#default_value' => (isset($items[$delta]->countries_map_svg)) ? $items[$delta]->countries_map_svg : "",
			'#empty_value' => '',
//			'#maxlength' => 100000,
			'#required' => $this->fieldDefinition->isRequired(),
			'#attributes' => ['class' => ['countries-map-data']],
		];
		
		$element['map_preview'] = [
			'#theme' => 'custom_map_position_area',
		];
		
		
		
		
		return $element;
	}
	
	
	public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
		foreach($values as $key => $value) {
			$values[$key]["countries_map_svg"] = $value["countries_map_svg"]["value"];
		}
		return $values;
	}
	
	/**
	 * Validate the color text field.
	 */
//	public static function validate($element, FormStateInterface $form_state) {
//		$value = $element['#value'];
//		if (strlen($value) == 0) {
//			$form_state->setValueForElement($element, '');
//			return;
//		}
//		if (!preg_match('/^#([a-f0-9]{6})$/iD', strtolower($value))) {
//			$form_state->setError($element, t("Color must be a 6-digit hexadecimal value, suitable for CSS."));
//		}
//	}
	
}