<?php

namespace Drupal\thales\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'field_example_text' widget.
 *
 * @FieldWidget(
 *   id = "custom_map_position_widget",
 *   module = "thales",
 *   label = @Translation("Position X et Y sur la Map"),
 *   field_types = { "thales_custom_map_position" }
 * )
 */
class ThalesWidgetExpert extends WidgetBase {
	
	/**
	 * {@inheritdoc}
	 */
	public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
		$element['#type'] = 'fieldset';
		
		
//		$element['#x_value'] = (isset($items[$delta]->x_value)) ? $items[$delta]->x_value : NULL;
//		$element['#y_value'] = (isset($items[$delta]->y_value)) ? $items[$delta]->y_value : NULL;
		
		$element['x_value'] = [
			'#type' => 'textfield',
			'#title' => $this->t('X Value'),
			'#default_value' => (isset($items[$delta]->x_value)) ? $items[$delta]->x_value : NULL,
			'#empty_value' => '',
			'#maxlength' => 255,
			'#required' => $this->fieldDefinition->isRequired(),
		];
		
		$element['y_value'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Y Value'),
			'#empty_value' => '',
			'#default_value' => (isset($items[$delta]->y_value)) ? $items[$delta]->y_value : NULL,
			'#maxlength' => 255,
			'#required' => $this->fieldDefinition->isRequired(),
		];
		
		$element['map_preview'] = [
			'#theme' => 'custom_map_position_expert',
		];
		
		
		
		
		return $element;
	}
	
	/**
	 * Validate the color text field.
	 */
//	public static function validate($element, FormStateInterface $form_state) {
//		$value = $element['#value'];
//		if (strlen($value) == 0) {
//			$form_state->setValueForElement($element, '');
//			return;
//		}
//		if (!preg_match('/^#([a-f0-9]{6})$/iD', strtolower($value))) {
//			$form_state->setError($element, t("Color must be a 6-digit hexadecimal value, suitable for CSS."));
//		}
//	}
	
}