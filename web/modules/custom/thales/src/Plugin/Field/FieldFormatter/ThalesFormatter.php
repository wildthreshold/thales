<?php

namespace Drupal\thales\Plugin\Field\FieldFormatter;


use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

use Drupal\rest_views\SerializedData;

/**
 * Plugin implementation of the 'Thales_default' formatter.
 *
 * @FieldFormatter(
 *   id = "custom_map_position_formatter",
 *   label = @Translation("Map position formater"),
 *   field_types = { "thales_custom_map_position" }
 * )
 */
class ThalesFormatter extends FormatterBase {
	
	/**
	 * {@inheritdoc}
	 */
	public function settingsSummary() {
		$summary = [];
		$summary[] = $this->t('Displays the Map position X, Y values.');
		return $summary;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function viewElements(FieldItemListInterface $items, $langcode) {
		$element = [];
		
		foreach ($items as $delta => $item) {
			// Render each element as markup.
			$data = [];
			$data["x_value"] = $item->x_value;
			$data["y_value"] = $item->y_value;
			$element[$delta] = [
//				'#markup' => $item->x_value . ', ' . $item->y_value,
				'#type' => 'data',
				'#data' => SerializedData::create($data),
//				'#theme' => 'custom_map_position_formatter',
//				'#x_value' => $item->x_value,
//				'#y_value' => $item->y_value,
			];
		}
		
		return $element;
	}
	
}