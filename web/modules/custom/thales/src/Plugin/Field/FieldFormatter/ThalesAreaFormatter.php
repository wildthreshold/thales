<?php

namespace Drupal\thales\Plugin\Field\FieldFormatter;


use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

use Drupal\rest_views\SerializedData;

/**
 * Plugin implementation of the 'Thales_default' formatter.
 *
 * @FieldFormatter(
 *   id = "thales_custom_countries_map_position_area_formatter",
 *   label = @Translation("Custom countries map position for areas formater"),
 *   field_types = { "thales_custom_countries_map_position_area" }
 * )
 */
class ThalesAreaFormatter extends FormatterBase {
	
	/**
	 * {@inheritdoc}
	 */
	public function settingsSummary() {
		$summary = [];
		$summary[] = $this->t('Displays countries on map');
		return $summary;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function viewElements(FieldItemListInterface $items, $langcode) {
		$element = [];
		
		foreach ($items as $delta => $item) {
			// Render each element as markup.
			$element[$delta] = [
				'#type' => 'processed_text',
				'#text' => $item->countries_map_svg,
				// '#plain_text' => $item->countries_map_svg,
				'#format' => 'full_html',
			];
//			$element[$delta] = [
//				'#markup' => $item->countries_map_svg,
//				'#type' => 'data',
//				'#data' => SerializedData::create($data),
//			];
		}
		
		return $element;
	}
	
}