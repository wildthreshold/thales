<?php

namespace Drupal\thales\Plugin\Field\FieldType;


use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;



/**
 * Provides a field type of baz.
 *
 * @FieldType(
 *   id = "thales_custom_map_position",
 *   label = @Translation("Custom map position"),
 *   description = @Translation("Permet de placer un point (X, Y) sur une map SVG."),
 *   default_formatter = "custom_map_position_formatter",
 *   default_widget = "custom_map_position_widget",
 * )
 */
class ThalesExpertItem extends FieldItemBase {
	
	
	/**
	 * {@inheritdoc}
	 */
	public static function schema(FieldStorageDefinitionInterface $field_definition) {
		return [
			// columns contains the values that the field will store
			'columns' => [
				// List the values that the field will save. This
				'x_value' => [
					'type' => 'float',
					'size' => 'big',
					'not null' => FALSE,
				],
				'y_value' => [
					'type' => 'float',
					'size' => 'big',
					'not null' => FALSE,
				],
			],
		];
	}
	
	
	/**
	 * {@inheritdoc}
	 */
	public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

		$properties['x_value'] = DataDefinition::create('float')->setLabel(t('X value'));
		$properties['y_value'] = DataDefinition::create('float')->setLabel(t('Y value'));
		
		return $properties;
	}
	
	
	
	/**
	 * {@inheritdoc}
	 */
	public function isEmpty() {
		$x_value = $this->get('x_value')->getValue();
		$y_value = $this->get('y_value')->getValue();
		return $x_value === NULL || $x_value === '' || $y_value === NULL || $y_value === '';;
	}
	
}