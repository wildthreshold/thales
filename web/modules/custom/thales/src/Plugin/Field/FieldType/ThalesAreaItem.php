<?php

namespace Drupal\thales\Plugin\Field\FieldType;


use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;



/**
 * Provides a field type of baz.
 *
 * @FieldType(
 *   id = "thales_custom_countries_map_position_area",
 *   label = @Translation("Custom countries map position for areas"),
 *   description = @Translation("Permet de placer des pays sur une map SVG."),
 *   default_formatter = "thales_custom_countries_map_position_area_formatter",
 *   default_widget = "thales_custom_countries_map_position_area_widget",
 * )
 */
class ThalesAreaItem extends FieldItemBase {
	
	
	/**
	 * {@inheritdoc}
	 */
	public static function schema(FieldStorageDefinitionInterface $field_definition) {
		return [
			// columns contains the values that the field will store
			'columns' => [
				// List the values that the field will save.
				'countries_map_svg' => [
					'type' => 'blob',
					'size' => 'big',
					'not null' => FALSE,
					'description' => 'Map SVG for area countries.',
				],
			],
		];
	}
	
	
	/**
	 * {@inheritdoc}
	 */
	public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

		$properties['countries_map_svg'] = DataDefinition::create('string')->setLabel(t('Map SVG for area countries'));
		
		return $properties;
	}
	
	
	
	/**
	 * {@inheritdoc}
	 */
	public function isEmpty() {
		$countries_map_svg = $this->get('countries_map_svg')->getValue();
		return $countries_map_svg === NULL || $countries_map_svg === '';
	}
	
}