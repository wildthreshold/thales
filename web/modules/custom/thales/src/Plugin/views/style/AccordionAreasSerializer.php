<?php

namespace Drupal\thales\Plugin\views\style;
use Drupal\rest\Plugin\views\style\Serializer;
use Drupal\file\Entity\File;

/**
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "accordion_areas_serializer",
 *   title = @Translation("Accordion For Areas Serializer"),
 *   help = @Translation("Custom serializer for Accordion Areas"),
 *   display_types = {"data"}
 * )
 */

class AccordionAreasSerializer extends Serializer {

    /**
     * TEST D'UNE VUE POUR AFFICHER LE TITRE ET L'IMAGES DU CHAMPS COUNTRIES IMBRIQUÉ DANS LE PARAGRAPH ACCORDION
     */

    public function render() {
        $rows = [];

        foreach ($this->view->result as $row_index => $row) {
            $this->view->row_index = $row_index;

            $rowAssoc = $this->serializer->normalize($this->view->rowPlugin->render($row)); //converting current row into array
            if (!empty($rowAssoc['countries_accordion'])) {
                foreach ($rowAssoc['countries_accordion'] as $i => $v) {
                    $term = \Drupal\taxonomy\Entity\Term::load($v);
                    // $country_img = file_create_url($term->get("field_flag_country")->entity->getFileUri());

                    if ($term->get("field_flag_country")->entity != null) {
                        $country_img = File::load($term->get("field_flag_country")->entity->get("fid")->value)->createFileUrl();
                    } else {
                        $country_img = "";
                    }
                    $country_fields = [
                        "name" => $term->name->value,
                        "flag_img" => $country_img,
                    ];
                    $rowAssoc['countries_accordion'][$i] = $country_fields;
                }
            }

            $rows[] = $rowAssoc;
        }

        unset($this->view->row_index);

        // Get the content type configured in the display or fallback to the default.
        if ((empty($this->view->live_preview))) {
            $content_type = $this->displayHandler->getContentType();
        }
        else {
            $content_type = !empty($this->options['formats']) ? reset($this->options['formats']) : 'json';
        }

        return $this->serializer->serialize($rows, $content_type, ['views_style_plugin' => $this]);
    }

}