<?php

namespace Drupal\thales\Plugin\views\style;
use Drupal\rest\Plugin\views\style\Serializer;
use Drupal\file\Entity\File;

/**
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "test_serializer",
 *   title = @Translation("Test Serializer"),
 *   help = @Translation("Custom serializer for testing"),
 *   display_types = {"data"}
 * )
 */

class TestSerializer extends Serializer {


    public function render() {
        $rows = [];

        foreach ($this->view->result as $row_index => $row) {
            $this->view->row_index = $row_index;

            $rowAssoc = $this->serializer->normalize($this->view->rowPlugin->render($row)); //converting current row into array

            /**
             * ...
             */

            $rows[] = $rowAssoc;
        }

        unset($this->view->row_index);

        // Get the content type configured in the display or fallback to the default.
        if ((empty($this->view->live_preview))) {
            $content_type = $this->displayHandler->getContentType();
        }
        else {
            $content_type = !empty($this->options['formats']) ? reset($this->options['formats']) : 'json';
        }

        return $this->serializer->serialize($rows, $content_type, ['views_style_plugin' => $this]);
    }

    // private function loadTerm($tid) {
    //     $term = \Drupal\taxonomy\Entity\Term::load($tid);

    //     $term_alias = \Drupal::service('path.alias_manager')->getAliasByPath('/taxonomy/term/' . $tid);
    //     $term->set('path', $term_alias);

    //     return $term;
    // }
}