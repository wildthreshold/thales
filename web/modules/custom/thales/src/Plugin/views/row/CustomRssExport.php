<?php

namespace Drupal\thales\Plugin\views\row;

use Drupal\views\Plugin\views\row\RssFields;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\thales\ThalesUtils;

/**
 * Renders an RSS item based on fields.
 *
 * @ViewsRow(
 *   id = "thales_rss_fields",
 *   title = @Translation("Custom RSS export Fields"),
 *   help = @Translation("Display fields as Modified"),
 *   theme = "views_view_row_rss",
 *   display_types = {"feed"}
 * )
 */
class CustomRssExport extends RssFields {
	
	/**
	 * Override of RssFields::render() with additional fields.
	 * @param object $row
	 * @return array
	 */
	public function render($row) {
		$build = parent::render($row);
		$item = $build['#row'];
		
		// Suppression contenu <dc:creator></dc:creator>
		$build['#row']->elements[1]["value"] = " ";
		
		// Modification du domaine dans <link></link>
		$link = explode("/", $build['#row']->link);
		$link = array_slice($link, 3);
		$build['#row']->link = ThalesUtils::APP_FRONT_STAGING . implode("/", $link);
		
//		$build['#row']->link = "https://app-front.teststaging.online" . strstr($build['#row']->link, '/news'); // STAGING
		
		return $build;
	}
}