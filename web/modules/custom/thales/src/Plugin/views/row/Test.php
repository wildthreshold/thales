<?php

namespace Drupal\thales\Plugin\views\row;

use Drupal\views\Plugin\views\row\RssFields;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
//use Drupal\views\ResultRow;

///**
// * Renders an RSS item based on fields.
// *
// * @ViewsRow(
// *   id = "mymodule_rss_fields",
// *   title = @Translation("Test Custom Fields"),
// *   help = @Translation("Display fields as RSS items."),
// *   theme = "views_view_row_rss",
// *   display_types = {"feed"}
// * )
// */
class Test extends RssFields {
	
	/**
	 * Override of RssFields::render() with additional fields.
	 *
	 * @param object $row
	 *
	 * @return array
	 */
	public function render($row) {
		$build = parent::render($row);
		$item = $build['#row'];
		
		// Add MRCTV nid
//		$item->elements[] = array(
//			'key' => 'source-nid',
//			'value' => $row->nid,
//		);
//
//		// Add channels and their target nids. We can get them from $row->_entity.
//		$site = $this->view->args[0];
//		// Get source nids from view.
//		$channel_tids = array_column($row->_entity->field_channels->getValue(), 'target_id');
//		// Now, get destination tids from config.
//		$mapping_config = \Drupal::config('video_export.mappings');
//		$all_mappings = $mapping_config->get('sites');
//
//		foreach($channel_tids as $source_channel) {
//			if(in_array($source_channel, array_keys($all_mappings[$site]['mappings']))) {
//				$item->elements[] = array(
//					'key' => $site . '-channel-map',
//					'value' => $all_mappings[$site]['mappings'][$source_channel],
//				);
//			}
//		}
		
		// Re-populate the $build array with the updated row.
//		$build['#row'] = $item;
		
		dump($build['#row']->nid);
		$build['#row']->link = "http://localhost:3000" . strstr($build['#row']->link, '/news');
//		$build['#row']->link = "toto";
//		dump($build);
//		$build['#view']->result = "tata";
//		$build['#row'] = "toto";
		
		return $build;
	}
}