<?php

namespace Drupal\thales\EventSubscriber;

use Drupal\jsonapi\ResourceType\ResourceTypeBuildEvents;
use Drupal\jsonapi\ResourceType\ResourceTypeBuildEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Drupal\Core\Render\Markup;
use Drupal\Component\Utility\Html;
use Drupal\Component\Render\MarkupInterface;

/**
 * Event subscriber to change some resource types.
 */
class CustomizeRestExport implements EventSubscriberInterface {

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents() {
        return [
//            ResourceTypeBuildEvents::BUILD => [
//                ['disableResourceType'],
//                ['aliasResourceTypeFields'],
//                ['disableResourceTypeFields'],
//                ['renameResourceType'],
//            ],
        ];
    }

    /**
     * Disables node/page resource type.
     * @param \Drupal\jsonapi\ResourceType\ResourceTypeBuildEvent $event
     *   The build event.
     */
//    public function disableResourceType(ResourceTypeBuildEvent $event) {
//        if ($event->getResourceTypeName() === 'node--page') {
//            $event->disableResourceType();
//        }
//    }

    /**
     * Aliases the body field to content.
     *
     * @param \Drupal\jsonapi\ResourceType\ResourceTypeBuildEvent $event
     *   The build event.
     */
//    public function aliasResourceTypeFields(ResourceTypeBuildEvent $event) {
//        if ($event->getResourceTypeName() === 'node--news') {
//            foreach ($event->getFields() as $field) {
//                if ($field->getInternalName() === 'field_content_news') {
//                    $event->setPublicFieldName($field, 'content');
//                }
//            }
//        }
//    }



    /**
     * Disables the sticky field on node--news.
     * @param \Drupal\jsonapi\ResourceType\ResourceTypeBuildEvent $event
     *   The build event.
     */
//    public function disableResourceTypeFields(ResourceTypeBuildEvent $event) {
//        if ($event->getResourceTypeName() === 'node--news') {
//            foreach ($event->getFields() as $field) {
//                if ($field->getInternalName() === 'sticky') {
//                    $event->disableField($field);
//                }
//            }
//        }
//    }



    /**
     * Renames node--news to news, exposing the resource as /jsonapi/news
     * @param \Drupal\jsonapi\ResourceType\ResourceTypeBuildEvent $event
     *   The build event.
     */
//    public function renameResourceType(ResourceTypeBuildEvent $event) {
//        if ($event->getResourceTypeName() === 'node--news') {
//            $event->setResourceTypeName('news');
//        }
//    }


}
