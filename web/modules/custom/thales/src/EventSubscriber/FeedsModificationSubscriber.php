<?php

namespace Drupal\thales\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\feeds\Event\EntityEvent;
use Drupal\feeds\Event\FeedsEvents;
use Drupal\feeds\Event\ParseEvent;



class FeedsModificationSubscriber implements EventSubscriberInterface {


    public function __construct() { }



    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents() {
        $events = [];
        $events[FeedsEvents::PARSE][] = 'parse';
        $events[FeedsEvents::PROCESS_ENTITY_PRESAVE][] = 'presave';
        $events[FeedsEvents::PROCESS_ENTITY_POSTSAVE][] = 'postsave';

        return $events;
    }


    /**
     * Fired when parsing has started.
     * @param Drupal\feeds\Event\ParseEvent; $event
     */
    public function parse (ParseEvent $event) {
        $feed = $event->getFeed();
        $toto = "toto";
    }



    /**
     * Fired before an entity is saved.
     * @param Drupal\feeds\Event\EntityEvent $event
     */
    public function presave(EntityEvent $event) {
//        dpm($event);
//        ddm($event);
        $feed = $event->getFeed(); // Get the feed object.
        $id = $feed->type->entity->id();
    }



    /**
     * Fired after an entity is saved.
     * @param Drupal\feeds\Event\EntityEvent $event
     */
    public function postsave(EntityEvent $event) {
        $feed = $event->getFeed();
        $id = $feed->type->entity->id();
    }


}